<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecordHistoriesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('record_histories', function (Blueprint $table) {
            $table->bigIncrements('history_id');
            $table->date('date');
            $table->text('ressume');
            $table->integer('estatus');
            $table->timestamps();
            $table->softDeletes();
            $table->engine = env('DB_ENGINE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('record_histories');
    }
}
