<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecordsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('records', function (Blueprint $table) {
            $table->bigIncrements('record_id');
            $table->timestamp('date');
            $table->string('city_nameid',150);
            $table->double('value');
            $table->string('metric_nameid',150);
            $table->bigInteger('history_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->engine = env('DB_ENGINE');
            $table->foreign('city_nameid')->references('nameid')->on('cities');
            $table->foreign('metric_nameid')->references('nameid')->on('metrics');
            $table->foreign('history_id')->references('history_id')->on('record_histories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('records');
    }
}
