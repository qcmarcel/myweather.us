<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLocationsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locations', function (Blueprint $table) {
            $table->bigIncrements('location_id');
            $table->double('latitude');
            $table->double('longitude');
            $table->double('area');
            $table->string('mapcode');
            $table->string('city_nameid',150);
            $table->timestamps();
            $table->softDeletes();
            $table->engine = env('DB_ENGINE');
            $table->foreign('city_nameid')->references('nameid')->on('cities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('locations');
    }
}
