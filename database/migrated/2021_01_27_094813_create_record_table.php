<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecordTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('record', function (Blueprint $table) {
            $table->increments('record_id');
            $table->timestamp('date');
            $table->string('city_nameid')->unsigned();
            $table->double('value');
            $table->string('metric_nameid')->unsigned();
            $table->bigInteger('history_id')->unsigned();
            $table->timestamps();
            $table->foreign('city_nameid')->references('nameid')->on('city');
            $table->foreign('metric_nameid')->references('nameid')->on('metric');
            $table->foreign('history_id')->references('id')->on('record_history');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('record');
    }
}
