<?php

namespace Database\Factories;

use App\Models\metric;
use Illuminate\Database\Eloquent\Factories\Factory;

class metricFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = metric::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'iso' => $this->faker->word,
        'display_name' => $this->faker->word,
        'estatus' => $this->faker->randomDigitNotNull,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
