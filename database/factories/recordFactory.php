<?php

namespace Database\Factories;

use App\Models\record;
use Illuminate\Database\Eloquent\Factories\Factory;

class recordFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = record::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'date' => $this->faker->date('Y-m-d H:i:s'),
        'city_nameid' => $this->faker->word,
        'value' => $this->faker->word,
        'metric_nameid' => $this->faker->word,
        'history_id' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
