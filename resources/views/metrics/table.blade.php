<div class="table-responsive-sm">
    <table class="table table-striped" id="metrics-table">
        <thead>
            <tr>
                <th>Nameid</th>
        <th>Iso</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($metrics as $metric)
            <tr>
                <td>{{ $metric->nameid }}</td>
            <td>{{ $metric->iso }}</td>
                <td>
                    {!! Form::open(['route' => ['metrics.destroy', $metric->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('metrics.show', [$metric->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('metrics.edit', [$metric->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>