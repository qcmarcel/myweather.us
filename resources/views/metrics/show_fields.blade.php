<!-- Nameid Field -->
<div class="form-group">
    {!! Form::label('nameid', 'Nameid:') !!}
    <p>{{ $metric->nameid }}</p>
</div>

<!-- Iso Field -->
<div class="form-group">
    {!! Form::label('iso', 'Iso:') !!}
    <p>{{ $metric->iso }}</p>
</div>

<!-- Display Name Field -->
<div class="form-group">
    {!! Form::label('display_name', 'Display Name:') !!}
    <p>{{ $metric->display_name }}</p>
</div>

<!-- Estatus Field -->
<div class="form-group">
    {!! Form::label('estatus', 'Estatus:') !!}
    <p>{{ $metric->estatus }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $metric->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $metric->updated_at }}</p>
</div>

