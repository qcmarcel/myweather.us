<!-- Latitude Field -->
<div class="form-group col-sm-6">
    {!! Form::label('latitude', 'Latitude:') !!}
    {!! Form::number('latitude', null, ['class' => 'form-control']) !!}
</div>

<!-- Longitude Field -->
<div class="form-group col-sm-6">
    {!! Form::label('longitude', 'Longitude:') !!}
    {!! Form::number('longitude', null, ['class' => 'form-control']) !!}
</div>

<!-- Area Field -->
<div class="form-group col-sm-6">
    {!! Form::label('area', 'Area:') !!}
    {!! Form::number('area', null, ['class' => 'form-control']) !!}
</div>

<!-- Mapcode Field -->
<div class="form-group col-sm-6">
    {!! Form::label('mapcode', 'Mapcode:') !!}
    {!! Form::text('mapcode', null, ['class' => 'form-control']) !!}
</div>

<!-- City Nameid Field -->
<div class="form-group col-sm-6">
    {!! Form::label('city_nameid', 'City Nameid:') !!}
    {!! Form::text('city_nameid', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('locations.index') }}" class="btn btn-secondary">Cancel</a>
</div>
