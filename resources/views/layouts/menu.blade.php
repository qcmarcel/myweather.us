<a href="/generator_builder"><i class="fa fa-cogs"></i> API generator</a>
<li class="nav-item {{ Request::is('cities*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('cities.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Cities</span>
    </a>
</li>
<li class="nav-item {{ Request::is('locations*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('locations.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Locations</span>
    </a>
</li>
<li class="nav-item {{ Request::is('metrics*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('metrics.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Metrics</span>
    </a>
</li>
<li class="nav-item {{ Request::is('recordHistories*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('recordHistories.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Record Histories</span>
    </a>
</li>
<li class="nav-item {{ Request::is('records*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('records.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Records</span>
    </a>
</li>
