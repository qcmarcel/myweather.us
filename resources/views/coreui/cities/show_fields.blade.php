<!-- Nameid Field -->
<div class="form-group">
    {!! Form::label('nameid', 'Nameid:') !!}
    <p>{{ $city->nameid }}</p>
</div>

<!-- Iso Field -->
<div class="form-group">
    {!! Form::label('iso', 'Iso:') !!}
    <p>{{ $city->iso }}</p>
</div>

<!-- Display Name Field -->
<div class="form-group">
    {!! Form::label('display_name', 'Display Name:') !!}
    <p>{{ $city->display_name }}</p>
</div>

<!-- Estatus Field -->
<div class="form-group">
    {!! Form::label('estatus', 'Estatus:') !!}
    <p>{{ $city->estatus }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $city->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $city->updated_at }}</p>
</div>

