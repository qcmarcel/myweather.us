<div class="table-responsive-sm">
    <table class="table table-striped" id="records-table">
        <thead>
            <tr>
                <th>Record Id</th>
        <th>City Nameid</th>
        <th>Metric Nameid</th>
        <th>History Id</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($records as $record)
            <tr>
                <td>{{ $record->record_id }}</td>
            <td>{{ $record->city_nameid }}</td>
            <td>{{ $record->metric_nameid }}</td>
            <td>{{ $record->history_id }}</td>
                <td>
                    {!! Form::open(['route' => ['coreui.records.destroy', $record->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('coreui.records.show', [$record->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('coreui.records.edit', [$record->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>