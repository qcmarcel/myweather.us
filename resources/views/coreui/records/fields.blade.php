<!-- Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date', 'Date:') !!}
    {!! Form::text('date', null, ['class' => 'form-control','id'=>'date']) !!}
</div>

@push('scripts')
   <script type="text/javascript">
           $('#date').datetimepicker({
               format: 'YYYY-MM-DD HH:mm:ss',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>
@endpush


<!-- City Nameid Field -->
<div class="form-group col-sm-6">
    {!! Form::label('city_nameid', 'City Nameid:') !!}
    {!! Form::text('city_nameid', null, ['class' => 'form-control']) !!}
</div>

<!-- Value Field -->
<div class="form-group col-sm-6">
    {!! Form::label('value', 'Value:') !!}
    {!! Form::number('value', null, ['class' => 'form-control']) !!}
</div>

<!-- Metric Nameid Field -->
<div class="form-group col-sm-6">
    {!! Form::label('metric_nameid', 'Metric Nameid:') !!}
    {!! Form::text('metric_nameid', null, ['class' => 'form-control']) !!}
</div>

<!-- History Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('history_id', 'History Id:') !!}
    {!! Form::number('history_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('coreui.records.index') }}" class="btn btn-secondary">Cancel</a>
</div>
