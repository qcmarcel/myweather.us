<!-- Record Id Field -->
<div class="form-group">
    {!! Form::label('record_id', 'Record Id:') !!}
    <p>{{ $record->record_id }}</p>
</div>

<!-- Date Field -->
<div class="form-group">
    {!! Form::label('date', 'Date:') !!}
    <p>{{ $record->date }}</p>
</div>

<!-- City Nameid Field -->
<div class="form-group">
    {!! Form::label('city_nameid', 'City Nameid:') !!}
    <p>{{ $record->city_nameid }}</p>
</div>

<!-- Value Field -->
<div class="form-group">
    {!! Form::label('value', 'Value:') !!}
    <p>{{ $record->value }}</p>
</div>

<!-- Metric Nameid Field -->
<div class="form-group">
    {!! Form::label('metric_nameid', 'Metric Nameid:') !!}
    <p>{{ $record->metric_nameid }}</p>
</div>

<!-- History Id Field -->
<div class="form-group">
    {!! Form::label('history_id', 'History Id:') !!}
    <p>{{ $record->history_id }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $record->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $record->updated_at }}</p>
</div>

