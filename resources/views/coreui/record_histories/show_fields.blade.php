<!-- History Id Field -->
<div class="form-group">
    {!! Form::label('history_id', 'History Id:') !!}
    <p>{{ $recordHistory->history_id }}</p>
</div>

<!-- Date Field -->
<div class="form-group">
    {!! Form::label('date', 'Date:') !!}
    <p>{{ $recordHistory->date }}</p>
</div>

<!-- Ressume Field -->
<div class="form-group">
    {!! Form::label('ressume', 'Ressume:') !!}
    <p>{{ $recordHistory->ressume }}</p>
</div>

<!-- Estatus Field -->
<div class="form-group">
    {!! Form::label('estatus', 'Estatus:') !!}
    <p>{{ $recordHistory->estatus }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $recordHistory->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $recordHistory->updated_at }}</p>
</div>

