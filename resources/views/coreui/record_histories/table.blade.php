<div class="table-responsive-sm">
    <table class="table table-striped" id="recordHistories-table">
        <thead>
            <tr>
                <th>History Id</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($recordHistories as $recordHistory)
            <tr>
                <td>{{ $recordHistory->history_id }}</td>
                <td>
                    {!! Form::open(['route' => ['coreui.recordHistories.destroy', $recordHistory->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('coreui.recordHistories.show', [$recordHistory->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('coreui.recordHistories.edit', [$recordHistory->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>