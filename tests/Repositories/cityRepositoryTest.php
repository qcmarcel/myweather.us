<?php namespace Tests\Repositories;

use App\Models\city;
use App\Repositories\cityRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class cityRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var cityRepository
     */
    protected $cityRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->cityRepo = \App::make(cityRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_city()
    {
        $city = city::factory()->make()->toArray();

        $createdcity = $this->cityRepo->create($city);

        $createdcity = $createdcity->toArray();
        $this->assertArrayHasKey('id', $createdcity);
        $this->assertNotNull($createdcity['id'], 'Created city must have id specified');
        $this->assertNotNull(city::find($createdcity['id']), 'city with given id must be in DB');
        $this->assertModelData($city, $createdcity);
    }

    /**
     * @test read
     */
    public function test_read_city()
    {
        $city = city::factory()->create();

        $dbcity = $this->cityRepo->find($city->id);

        $dbcity = $dbcity->toArray();
        $this->assertModelData($city->toArray(), $dbcity);
    }

    /**
     * @test update
     */
    public function test_update_city()
    {
        $city = city::factory()->create();
        $fakecity = city::factory()->make()->toArray();

        $updatedcity = $this->cityRepo->update($fakecity, $city->id);

        $this->assertModelData($fakecity, $updatedcity->toArray());
        $dbcity = $this->cityRepo->find($city->id);
        $this->assertModelData($fakecity, $dbcity->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_city()
    {
        $city = city::factory()->create();

        $resp = $this->cityRepo->delete($city->id);

        $this->assertTrue($resp);
        $this->assertNull(city::find($city->id), 'city should not exist in DB');
    }
}
