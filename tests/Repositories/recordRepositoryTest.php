<?php namespace Tests\Repositories;

use App\Models\record;
use App\Repositories\recordRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class recordRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var recordRepository
     */
    protected $recordRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->recordRepo = \App::make(recordRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_record()
    {
        $record = record::factory()->make()->toArray();

        $createdrecord = $this->recordRepo->create($record);

        $createdrecord = $createdrecord->toArray();
        $this->assertArrayHasKey('id', $createdrecord);
        $this->assertNotNull($createdrecord['id'], 'Created record must have id specified');
        $this->assertNotNull(record::find($createdrecord['id']), 'record with given id must be in DB');
        $this->assertModelData($record, $createdrecord);
    }

    /**
     * @test read
     */
    public function test_read_record()
    {
        $record = record::factory()->create();

        $dbrecord = $this->recordRepo->find($record->id);

        $dbrecord = $dbrecord->toArray();
        $this->assertModelData($record->toArray(), $dbrecord);
    }

    /**
     * @test update
     */
    public function test_update_record()
    {
        $record = record::factory()->create();
        $fakerecord = record::factory()->make()->toArray();

        $updatedrecord = $this->recordRepo->update($fakerecord, $record->id);

        $this->assertModelData($fakerecord, $updatedrecord->toArray());
        $dbrecord = $this->recordRepo->find($record->id);
        $this->assertModelData($fakerecord, $dbrecord->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_record()
    {
        $record = record::factory()->create();

        $resp = $this->recordRepo->delete($record->id);

        $this->assertTrue($resp);
        $this->assertNull(record::find($record->id), 'record should not exist in DB');
    }
}
