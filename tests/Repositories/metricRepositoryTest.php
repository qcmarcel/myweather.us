<?php namespace Tests\Repositories;

use App\Models\metric;
use App\Repositories\metricRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class metricRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var metricRepository
     */
    protected $metricRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->metricRepo = \App::make(metricRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_metric()
    {
        $metric = metric::factory()->make()->toArray();

        $createdmetric = $this->metricRepo->create($metric);

        $createdmetric = $createdmetric->toArray();
        $this->assertArrayHasKey('id', $createdmetric);
        $this->assertNotNull($createdmetric['id'], 'Created metric must have id specified');
        $this->assertNotNull(metric::find($createdmetric['id']), 'metric with given id must be in DB');
        $this->assertModelData($metric, $createdmetric);
    }

    /**
     * @test read
     */
    public function test_read_metric()
    {
        $metric = metric::factory()->create();

        $dbmetric = $this->metricRepo->find($metric->id);

        $dbmetric = $dbmetric->toArray();
        $this->assertModelData($metric->toArray(), $dbmetric);
    }

    /**
     * @test update
     */
    public function test_update_metric()
    {
        $metric = metric::factory()->create();
        $fakemetric = metric::factory()->make()->toArray();

        $updatedmetric = $this->metricRepo->update($fakemetric, $metric->id);

        $this->assertModelData($fakemetric, $updatedmetric->toArray());
        $dbmetric = $this->metricRepo->find($metric->id);
        $this->assertModelData($fakemetric, $dbmetric->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_metric()
    {
        $metric = metric::factory()->create();

        $resp = $this->metricRepo->delete($metric->id);

        $this->assertTrue($resp);
        $this->assertNull(metric::find($metric->id), 'metric should not exist in DB');
    }
}
