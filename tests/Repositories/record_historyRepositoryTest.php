<?php namespace Tests\Repositories;

use App\Models\record_history;
use App\Repositories\record_historyRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class record_historyRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var record_historyRepository
     */
    protected $recordHistoryRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->recordHistoryRepo = \App::make(record_historyRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_record_history()
    {
        $recordHistory = record_history::factory()->make()->toArray();

        $createdrecord_history = $this->recordHistoryRepo->create($recordHistory);

        $createdrecord_history = $createdrecord_history->toArray();
        $this->assertArrayHasKey('id', $createdrecord_history);
        $this->assertNotNull($createdrecord_history['id'], 'Created record_history must have id specified');
        $this->assertNotNull(record_history::find($createdrecord_history['id']), 'record_history with given id must be in DB');
        $this->assertModelData($recordHistory, $createdrecord_history);
    }

    /**
     * @test read
     */
    public function test_read_record_history()
    {
        $recordHistory = record_history::factory()->create();

        $dbrecord_history = $this->recordHistoryRepo->find($recordHistory->id);

        $dbrecord_history = $dbrecord_history->toArray();
        $this->assertModelData($recordHistory->toArray(), $dbrecord_history);
    }

    /**
     * @test update
     */
    public function test_update_record_history()
    {
        $recordHistory = record_history::factory()->create();
        $fakerecord_history = record_history::factory()->make()->toArray();

        $updatedrecord_history = $this->recordHistoryRepo->update($fakerecord_history, $recordHistory->id);

        $this->assertModelData($fakerecord_history, $updatedrecord_history->toArray());
        $dbrecord_history = $this->recordHistoryRepo->find($recordHistory->id);
        $this->assertModelData($fakerecord_history, $dbrecord_history->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_record_history()
    {
        $recordHistory = record_history::factory()->create();

        $resp = $this->recordHistoryRepo->delete($recordHistory->id);

        $this->assertTrue($resp);
        $this->assertNull(record_history::find($recordHistory->id), 'record_history should not exist in DB');
    }
}
