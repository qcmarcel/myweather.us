<?php namespace Tests\Repositories;

use App\Models\location;
use App\Repositories\locationRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class locationRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var locationRepository
     */
    protected $locationRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->locationRepo = \App::make(locationRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_location()
    {
        $location = location::factory()->make()->toArray();

        $createdlocation = $this->locationRepo->create($location);

        $createdlocation = $createdlocation->toArray();
        $this->assertArrayHasKey('id', $createdlocation);
        $this->assertNotNull($createdlocation['id'], 'Created location must have id specified');
        $this->assertNotNull(location::find($createdlocation['id']), 'location with given id must be in DB');
        $this->assertModelData($location, $createdlocation);
    }

    /**
     * @test read
     */
    public function test_read_location()
    {
        $location = location::factory()->create();

        $dblocation = $this->locationRepo->find($location->id);

        $dblocation = $dblocation->toArray();
        $this->assertModelData($location->toArray(), $dblocation);
    }

    /**
     * @test update
     */
    public function test_update_location()
    {
        $location = location::factory()->create();
        $fakelocation = location::factory()->make()->toArray();

        $updatedlocation = $this->locationRepo->update($fakelocation, $location->id);

        $this->assertModelData($fakelocation, $updatedlocation->toArray());
        $dblocation = $this->locationRepo->find($location->id);
        $this->assertModelData($fakelocation, $dblocation->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_location()
    {
        $location = location::factory()->create();

        $resp = $this->locationRepo->delete($location->id);

        $this->assertTrue($resp);
        $this->assertNull(location::find($location->id), 'location should not exist in DB');
    }
}
