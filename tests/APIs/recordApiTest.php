<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\record;

class recordApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_record()
    {
        $record = record::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/records', $record
        );

        $this->assertApiResponse($record);
    }

    /**
     * @test
     */
    public function test_read_record()
    {
        $record = record::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/records/'.$record->id
        );

        $this->assertApiResponse($record->toArray());
    }

    /**
     * @test
     */
    public function test_update_record()
    {
        $record = record::factory()->create();
        $editedrecord = record::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/records/'.$record->id,
            $editedrecord
        );

        $this->assertApiResponse($editedrecord);
    }

    /**
     * @test
     */
    public function test_delete_record()
    {
        $record = record::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/records/'.$record->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/records/'.$record->id
        );

        $this->response->assertStatus(404);
    }
}
