<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\record_history;

class record_historyApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_record_history()
    {
        $recordHistory = record_history::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/record_histories', $recordHistory
        );

        $this->assertApiResponse($recordHistory);
    }

    /**
     * @test
     */
    public function test_read_record_history()
    {
        $recordHistory = record_history::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/record_histories/'.$recordHistory->id
        );

        $this->assertApiResponse($recordHistory->toArray());
    }

    /**
     * @test
     */
    public function test_update_record_history()
    {
        $recordHistory = record_history::factory()->create();
        $editedrecord_history = record_history::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/record_histories/'.$recordHistory->id,
            $editedrecord_history
        );

        $this->assertApiResponse($editedrecord_history);
    }

    /**
     * @test
     */
    public function test_delete_record_history()
    {
        $recordHistory = record_history::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/record_histories/'.$recordHistory->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/record_histories/'.$recordHistory->id
        );

        $this->response->assertStatus(404);
    }
}
