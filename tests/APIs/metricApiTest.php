<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\metric;

class metricApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_metric()
    {
        $metric = metric::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/metrics', $metric
        );

        $this->assertApiResponse($metric);
    }

    /**
     * @test
     */
    public function test_read_metric()
    {
        $metric = metric::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/metrics/'.$metric->id
        );

        $this->assertApiResponse($metric->toArray());
    }

    /**
     * @test
     */
    public function test_update_metric()
    {
        $metric = metric::factory()->create();
        $editedmetric = metric::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/metrics/'.$metric->id,
            $editedmetric
        );

        $this->assertApiResponse($editedmetric);
    }

    /**
     * @test
     */
    public function test_delete_metric()
    {
        $metric = metric::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/metrics/'.$metric->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/metrics/'.$metric->id
        );

        $this->response->assertStatus(404);
    }
}
