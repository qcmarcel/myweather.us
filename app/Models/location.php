<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class location
 * @package App\Models
 * @version January 27, 2021, 10:12 am UTC
 *
 * @property number $latitude
 * @property number $longitude
 * @property number $area
 * @property string $mapcode
 * @property string $city_nameid
 */
class location extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'locations';
    

    protected $dates = ['deleted_at'];


    protected $primaryKey = 'location_id';

    public $fillable = [
        'latitude',
        'longitude',
        'area',
        'mapcode',
        'city_nameid'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'location_id' => 'integer',
        'latitude' => 'double',
        'longitude' => 'double',
        'area' => 'double',
        'mapcode' => 'string',
        'city_nameid' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
