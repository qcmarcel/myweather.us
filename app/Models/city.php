<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class city
 * @package App\Models
 * @version January 27, 2021, 10:05 am UTC
 *
 * @property string $nameid
 * @property string $iso
 * @property string $display_name
 * @property integer $estatus
 */
class city extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'cities';
    

    protected $dates = ['deleted_at'];


    protected $primaryKey = 'nameid';

    public $fillable = [
        'nameid',
        'iso',
        'display_name',
        'estatus'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'nameid' => 'string',
        'iso' => 'string',
        'display_name' => 'string',
        'estatus' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
