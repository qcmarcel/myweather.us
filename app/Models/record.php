<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class record
 * @package App\Models
 * @version January 27, 2021, 10:14 am UTC
 *
 * @property string $date
 * @property string $city_nameid
 * @property number $value
 * @property string $metric_nameid
 * @property integer $history_id
 */
class record extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'records';
    

    protected $dates = ['deleted_at'];


    protected $primaryKey = 'record_id';

    public $fillable = [
        'date',
        'city_nameid',
        'value',
        'metric_nameid',
        'history_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'record_id' => 'integer',
        'city_nameid' => 'string',
        'value' => 'double',
        'metric_nameid' => 'string',
        'history_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
