<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class record_history
 * @package App\Models
 * @version January 27, 2021, 10:14 am UTC
 *
 * @property string $date
 * @property string $ressume
 * @property integer $estatus
 */
class record_history extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'record_histories';
    

    protected $dates = ['deleted_at'];


    protected $primaryKey = 'history_id';

    public $fillable = [
        'date',
        'ressume',
        'estatus'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'history_id' => 'integer',
        'date' => 'date',
        'ressume' => 'string',
        'estatus' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
