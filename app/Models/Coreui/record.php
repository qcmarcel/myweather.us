<?php

namespace App\Models\Coreui;

use Eloquent as Model;



/**
 * Class record
 * @package App\Models\Coreui
 * @version January 27, 2021, 9:48 am UTC
 *
 * @property string $date
 * @property string $city_nameid
 * @property number $value
 * @property string $metric_nameid
 * @property integer $history_id
 */
class record extends Model
{


    public $table = 'record';
    



    public $fillable = [
        'date',
        'city_nameid',
        'value',
        'metric_nameid',
        'history_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'record_id' => 'integer',
        'city_nameid' => 'string',
        'value' => 'double',
        'metric_nameid' => 'string',
        'history_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
