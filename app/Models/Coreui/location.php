<?php

namespace App\Models\Coreui;

use Eloquent as Model;



/**
 * Class location
 * @package App\Models\Coreui
 * @version January 27, 2021, 9:35 am UTC
 *
 * @property number $latitude
 * @property number $longitude
 * @property number $area
 * @property string $mapcode
 * @property string $city_nameid
 */
class location extends Model
{


    public $table = 'location';
    



    public $fillable = [
        'latitude',
        'longitude',
        'area',
        'mapcode',
        'city_nameid'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'location_id' => 'integer',
        'latitude' => 'double',
        'longitude' => 'double',
        'area' => 'double',
        'mapcode' => 'string',
        'city_nameid' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
