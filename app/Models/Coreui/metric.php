<?php

namespace App\Models\Coreui;

use Eloquent as Model;



/**
 * Class metric
 * @package App\Models\Coreui
 * @version January 27, 2021, 9:39 am UTC
 *
 * @property string $nameid
 * @property string $iso
 * @property string $display_name
 * @property integer $estatus
 */
class metric extends Model
{


    public $table = 'metric';
    



    public $fillable = [
        'nameid',
        'iso',
        'display_name',
        'estatus'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'nameid' => 'string',
        'iso' => 'string',
        'display_name' => 'string',
        'estatus' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
