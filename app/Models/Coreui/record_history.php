<?php

namespace App\Models\Coreui;

use Eloquent as Model;



/**
 * Class record_history
 * @package App\Models\Coreui
 * @version January 27, 2021, 9:42 am UTC
 *
 * @property string $date
 * @property string $ressume
 * @property integer $estatus
 */
class record_history extends Model
{


    public $table = 'record_history';
    



    public $fillable = [
        'date',
        'ressume',
        'estatus'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'history_id' => 'integer',
        'date' => 'date',
        'ressume' => 'string',
        'estatus' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
