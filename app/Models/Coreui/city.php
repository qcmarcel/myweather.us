<?php

namespace App\Models\Coreui;

use Eloquent as Model;



/**
 * Class city
 * @package App\Models\Coreui
 * @version January 27, 2021, 9:28 am UTC
 *
 * @property string $nameid
 * @property string $iso
 * @property string $display_name
 * @property integer $estatus
 */
class city extends Model
{


    public $table = 'city';
    



    public $fillable = [
        'nameid',
        'iso',
        'display_name',
        'estatus'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'nameid' => 'string',
        'iso' => 'string',
        'display_name' => 'string',
        'estatus' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
