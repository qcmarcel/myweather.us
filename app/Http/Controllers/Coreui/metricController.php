<?php

namespace App\Http\Controllers\Coreui;

use App\Http\Requests\Coreui\CreatemetricRequest;
use App\Http\Requests\Coreui\UpdatemetricRequest;
use App\Repositories\Coreui\metricRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class metricController extends AppBaseController
{
    /** @var  metricRepository */
    private $metricRepository;

    public function __construct(metricRepository $metricRepo)
    {
        $this->metricRepository = $metricRepo;
    }

    /**
     * Display a listing of the metric.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $metrics = $this->metricRepository->paginate(10);

        return view('coreui.metrics.index')
            ->with('metrics', $metrics);
    }

    /**
     * Show the form for creating a new metric.
     *
     * @return Response
     */
    public function create()
    {
        return view('coreui.metrics.create');
    }

    /**
     * Store a newly created metric in storage.
     *
     * @param CreatemetricRequest $request
     *
     * @return Response
     */
    public function store(CreatemetricRequest $request)
    {
        $input = $request->all();

        $metric = $this->metricRepository->create($input);

        Flash::success('Metric saved successfully.');

        return redirect(route('coreui.metrics.index'));
    }

    /**
     * Display the specified metric.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $metric = $this->metricRepository->find($id);

        if (empty($metric)) {
            Flash::error('Metric not found');

            return redirect(route('coreui.metrics.index'));
        }

        return view('coreui.metrics.show')->with('metric', $metric);
    }

    /**
     * Show the form for editing the specified metric.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $metric = $this->metricRepository->find($id);

        if (empty($metric)) {
            Flash::error('Metric not found');

            return redirect(route('coreui.metrics.index'));
        }

        return view('coreui.metrics.edit')->with('metric', $metric);
    }

    /**
     * Update the specified metric in storage.
     *
     * @param int $id
     * @param UpdatemetricRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatemetricRequest $request)
    {
        $metric = $this->metricRepository->find($id);

        if (empty($metric)) {
            Flash::error('Metric not found');

            return redirect(route('coreui.metrics.index'));
        }

        $metric = $this->metricRepository->update($request->all(), $id);

        Flash::success('Metric updated successfully.');

        return redirect(route('coreui.metrics.index'));
    }

    /**
     * Remove the specified metric from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $metric = $this->metricRepository->find($id);

        if (empty($metric)) {
            Flash::error('Metric not found');

            return redirect(route('coreui.metrics.index'));
        }

        $this->metricRepository->delete($id);

        Flash::success('Metric deleted successfully.');

        return redirect(route('coreui.metrics.index'));
    }
}
