<?php

namespace App\Http\Controllers\Coreui;

use App\Http\Requests\Coreui\Createrecord_historyRequest;
use App\Http\Requests\Coreui\Updaterecord_historyRequest;
use App\Repositories\Coreui\record_historyRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class record_historyController extends AppBaseController
{
    /** @var  record_historyRepository */
    private $recordHistoryRepository;

    public function __construct(record_historyRepository $recordHistoryRepo)
    {
        $this->recordHistoryRepository = $recordHistoryRepo;
    }

    /**
     * Display a listing of the record_history.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $recordHistories = $this->recordHistoryRepository->paginate(10);

        return view('coreui.record_histories.index')
            ->with('recordHistories', $recordHistories);
    }

    /**
     * Show the form for creating a new record_history.
     *
     * @return Response
     */
    public function create()
    {
        return view('coreui.record_histories.create');
    }

    /**
     * Store a newly created record_history in storage.
     *
     * @param Createrecord_historyRequest $request
     *
     * @return Response
     */
    public function store(Createrecord_historyRequest $request)
    {
        $input = $request->all();

        $recordHistory = $this->recordHistoryRepository->create($input);

        Flash::success('Record History saved successfully.');

        return redirect(route('coreui.recordHistories.index'));
    }

    /**
     * Display the specified record_history.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $recordHistory = $this->recordHistoryRepository->find($id);

        if (empty($recordHistory)) {
            Flash::error('Record History not found');

            return redirect(route('coreui.recordHistories.index'));
        }

        return view('coreui.record_histories.show')->with('recordHistory', $recordHistory);
    }

    /**
     * Show the form for editing the specified record_history.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $recordHistory = $this->recordHistoryRepository->find($id);

        if (empty($recordHistory)) {
            Flash::error('Record History not found');

            return redirect(route('coreui.recordHistories.index'));
        }

        return view('coreui.record_histories.edit')->with('recordHistory', $recordHistory);
    }

    /**
     * Update the specified record_history in storage.
     *
     * @param int $id
     * @param Updaterecord_historyRequest $request
     *
     * @return Response
     */
    public function update($id, Updaterecord_historyRequest $request)
    {
        $recordHistory = $this->recordHistoryRepository->find($id);

        if (empty($recordHistory)) {
            Flash::error('Record History not found');

            return redirect(route('coreui.recordHistories.index'));
        }

        $recordHistory = $this->recordHistoryRepository->update($request->all(), $id);

        Flash::success('Record History updated successfully.');

        return redirect(route('coreui.recordHistories.index'));
    }

    /**
     * Remove the specified record_history from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $recordHistory = $this->recordHistoryRepository->find($id);

        if (empty($recordHistory)) {
            Flash::error('Record History not found');

            return redirect(route('coreui.recordHistories.index'));
        }

        $this->recordHistoryRepository->delete($id);

        Flash::success('Record History deleted successfully.');

        return redirect(route('coreui.recordHistories.index'));
    }
}
