<?php

namespace App\Http\Controllers\Coreui;

use App\Http\Requests\Coreui\CreatelocationRequest;
use App\Http\Requests\Coreui\UpdatelocationRequest;
use App\Repositories\Coreui\locationRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class locationController extends AppBaseController
{
    /** @var  locationRepository */
    private $locationRepository;

    public function __construct(locationRepository $locationRepo)
    {
        $this->locationRepository = $locationRepo;
    }

    /**
     * Display a listing of the location.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $locations = $this->locationRepository->paginate(10);

        return view('coreui.locations.index')
            ->with('locations', $locations);
    }

    /**
     * Show the form for creating a new location.
     *
     * @return Response
     */
    public function create()
    {
        return view('coreui.locations.create');
    }

    /**
     * Store a newly created location in storage.
     *
     * @param CreatelocationRequest $request
     *
     * @return Response
     */
    public function store(CreatelocationRequest $request)
    {
        $input = $request->all();

        $location = $this->locationRepository->create($input);

        Flash::success('Location saved successfully.');

        return redirect(route('coreui.locations.index'));
    }

    /**
     * Display the specified location.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $location = $this->locationRepository->find($id);

        if (empty($location)) {
            Flash::error('Location not found');

            return redirect(route('coreui.locations.index'));
        }

        return view('coreui.locations.show')->with('location', $location);
    }

    /**
     * Show the form for editing the specified location.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $location = $this->locationRepository->find($id);

        if (empty($location)) {
            Flash::error('Location not found');

            return redirect(route('coreui.locations.index'));
        }

        return view('coreui.locations.edit')->with('location', $location);
    }

    /**
     * Update the specified location in storage.
     *
     * @param int $id
     * @param UpdatelocationRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatelocationRequest $request)
    {
        $location = $this->locationRepository->find($id);

        if (empty($location)) {
            Flash::error('Location not found');

            return redirect(route('coreui.locations.index'));
        }

        $location = $this->locationRepository->update($request->all(), $id);

        Flash::success('Location updated successfully.');

        return redirect(route('coreui.locations.index'));
    }

    /**
     * Remove the specified location from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $location = $this->locationRepository->find($id);

        if (empty($location)) {
            Flash::error('Location not found');

            return redirect(route('coreui.locations.index'));
        }

        $this->locationRepository->delete($id);

        Flash::success('Location deleted successfully.');

        return redirect(route('coreui.locations.index'));
    }
}
