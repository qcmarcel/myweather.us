<?php

namespace App\Http\Controllers\Coreui;

use App\Http\Requests\Coreui\CreatecityRequest;
use App\Http\Requests\Coreui\UpdatecityRequest;
use App\Repositories\Coreui\cityRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class cityController extends AppBaseController
{
    /** @var  cityRepository */
    private $cityRepository;

    public function __construct(cityRepository $cityRepo)
    {
        $this->cityRepository = $cityRepo;
    }

    /**
     * Display a listing of the city.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $cities = $this->cityRepository->paginate(10);

        return view('coreui.cities.index')
            ->with('cities', $cities);
    }

    /**
     * Show the form for creating a new city.
     *
     * @return Response
     */
    public function create()
    {
        return view('coreui.cities.create');
    }

    /**
     * Store a newly created city in storage.
     *
     * @param CreatecityRequest $request
     *
     * @return Response
     */
    public function store(CreatecityRequest $request)
    {
        $input = $request->all();

        $city = $this->cityRepository->create($input);

        Flash::success('City saved successfully.');

        return redirect(route('coreui.cities.index'));
    }

    /**
     * Display the specified city.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $city = $this->cityRepository->find($id);

        if (empty($city)) {
            Flash::error('City not found');

            return redirect(route('coreui.cities.index'));
        }

        return view('coreui.cities.show')->with('city', $city);
    }

    /**
     * Show the form for editing the specified city.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $city = $this->cityRepository->find($id);

        if (empty($city)) {
            Flash::error('City not found');

            return redirect(route('coreui.cities.index'));
        }

        return view('coreui.cities.edit')->with('city', $city);
    }

    /**
     * Update the specified city in storage.
     *
     * @param int $id
     * @param UpdatecityRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatecityRequest $request)
    {
        $city = $this->cityRepository->find($id);

        if (empty($city)) {
            Flash::error('City not found');

            return redirect(route('coreui.cities.index'));
        }

        $city = $this->cityRepository->update($request->all(), $id);

        Flash::success('City updated successfully.');

        return redirect(route('coreui.cities.index'));
    }

    /**
     * Remove the specified city from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $city = $this->cityRepository->find($id);

        if (empty($city)) {
            Flash::error('City not found');

            return redirect(route('coreui.cities.index'));
        }

        $this->cityRepository->delete($id);

        Flash::success('City deleted successfully.');

        return redirect(route('coreui.cities.index'));
    }
}
