<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreaterecordRequest;
use App\Http\Requests\UpdaterecordRequest;
use App\Repositories\recordRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class recordController extends AppBaseController
{
    /** @var  recordRepository */
    private $recordRepository;

    public function __construct(recordRepository $recordRepo)
    {
        $this->recordRepository = $recordRepo;
    }

    /**
     * Display a listing of the record.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $records = $this->recordRepository->all();

        return view('records.index')
            ->with('records', $records);
    }

    /**
     * Show the form for creating a new record.
     *
     * @return Response
     */
    public function create()
    {
        return view('records.create');
    }

    /**
     * Store a newly created record in storage.
     *
     * @param CreaterecordRequest $request
     *
     * @return Response
     */
    public function store(CreaterecordRequest $request)
    {
        $input = $request->all();

        $record = $this->recordRepository->create($input);

        Flash::success('Record saved successfully.');

        return redirect(route('records.index'));
    }

    /**
     * Display the specified record.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $record = $this->recordRepository->find($id);

        if (empty($record)) {
            Flash::error('Record not found');

            return redirect(route('records.index'));
        }

        return view('records.show')->with('record', $record);
    }

    /**
     * Show the form for editing the specified record.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $record = $this->recordRepository->find($id);

        if (empty($record)) {
            Flash::error('Record not found');

            return redirect(route('records.index'));
        }

        return view('records.edit')->with('record', $record);
    }

    /**
     * Update the specified record in storage.
     *
     * @param int $id
     * @param UpdaterecordRequest $request
     *
     * @return Response
     */
    public function update($id, UpdaterecordRequest $request)
    {
        $record = $this->recordRepository->find($id);

        if (empty($record)) {
            Flash::error('Record not found');

            return redirect(route('records.index'));
        }

        $record = $this->recordRepository->update($request->all(), $id);

        Flash::success('Record updated successfully.');

        return redirect(route('records.index'));
    }

    /**
     * Remove the specified record from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $record = $this->recordRepository->find($id);

        if (empty($record)) {
            Flash::error('Record not found');

            return redirect(route('records.index'));
        }

        $this->recordRepository->delete($id);

        Flash::success('Record deleted successfully.');

        return redirect(route('records.index'));
    }
}
