<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatelocationAPIRequest;
use App\Http\Requests\API\UpdatelocationAPIRequest;
use App\Models\location;
use App\Repositories\locationRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\locationResource;
use Response;

/**
 * Class locationController
 * @package App\Http\Controllers\API
 */

class locationAPIController extends AppBaseController
{
    /** @var  locationRepository */
    private $locationRepository;

    public function __construct(locationRepository $locationRepo)
    {
        $this->locationRepository = $locationRepo;
    }

    /**
     * Display a listing of the location.
     * GET|HEAD /locations
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $locations = $this->locationRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(locationResource::collection($locations), 'Locations retrieved successfully');
    }

    /**
     * Store a newly created location in storage.
     * POST /locations
     *
     * @param CreatelocationAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatelocationAPIRequest $request)
    {
        $input = $request->all();

        $location = $this->locationRepository->create($input);

        return $this->sendResponse(new locationResource($location), 'Location saved successfully');
    }

    /**
     * Display the specified location.
     * GET|HEAD /locations/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var location $location */
        $location = $this->locationRepository->find($id);

        if (empty($location)) {
            return $this->sendError('Location not found');
        }

        return $this->sendResponse(new locationResource($location), 'Location retrieved successfully');
    }

    /**
     * Update the specified location in storage.
     * PUT/PATCH /locations/{id}
     *
     * @param int $id
     * @param UpdatelocationAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatelocationAPIRequest $request)
    {
        $input = $request->all();

        /** @var location $location */
        $location = $this->locationRepository->find($id);

        if (empty($location)) {
            return $this->sendError('Location not found');
        }

        $location = $this->locationRepository->update($input, $id);

        return $this->sendResponse(new locationResource($location), 'location updated successfully');
    }

    /**
     * Remove the specified location from storage.
     * DELETE /locations/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var location $location */
        $location = $this->locationRepository->find($id);

        if (empty($location)) {
            return $this->sendError('Location not found');
        }

        $location->delete();

        return $this->sendSuccess('Location deleted successfully');
    }
}
