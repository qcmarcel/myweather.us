<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatemetricAPIRequest;
use App\Http\Requests\API\UpdatemetricAPIRequest;
use App\Models\metric;
use App\Repositories\metricRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\metricResource;
use Response;

/**
 * Class metricController
 * @package App\Http\Controllers\API
 */

class metricAPIController extends AppBaseController
{
    /** @var  metricRepository */
    private $metricRepository;

    public function __construct(metricRepository $metricRepo)
    {
        $this->metricRepository = $metricRepo;
    }

    /**
     * Display a listing of the metric.
     * GET|HEAD /metrics
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $metrics = $this->metricRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(metricResource::collection($metrics), 'Metrics retrieved successfully');
    }

    /**
     * Store a newly created metric in storage.
     * POST /metrics
     *
     * @param CreatemetricAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatemetricAPIRequest $request)
    {
        $input = $request->all();

        $metric = $this->metricRepository->create($input);

        return $this->sendResponse(new metricResource($metric), 'Metric saved successfully');
    }

    /**
     * Display the specified metric.
     * GET|HEAD /metrics/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var metric $metric */
        $metric = $this->metricRepository->find($id);

        if (empty($metric)) {
            return $this->sendError('Metric not found');
        }

        return $this->sendResponse(new metricResource($metric), 'Metric retrieved successfully');
    }

    /**
     * Update the specified metric in storage.
     * PUT/PATCH /metrics/{id}
     *
     * @param int $id
     * @param UpdatemetricAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatemetricAPIRequest $request)
    {
        $input = $request->all();

        /** @var metric $metric */
        $metric = $this->metricRepository->find($id);

        if (empty($metric)) {
            return $this->sendError('Metric not found');
        }

        $metric = $this->metricRepository->update($input, $id);

        return $this->sendResponse(new metricResource($metric), 'metric updated successfully');
    }

    /**
     * Remove the specified metric from storage.
     * DELETE /metrics/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var metric $metric */
        $metric = $this->metricRepository->find($id);

        if (empty($metric)) {
            return $this->sendError('Metric not found');
        }

        $metric->delete();

        return $this->sendSuccess('Metric deleted successfully');
    }
}
