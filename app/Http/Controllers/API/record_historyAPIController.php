<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createrecord_historyAPIRequest;
use App\Http\Requests\API\Updaterecord_historyAPIRequest;
use App\Models\record_history;
use App\Repositories\record_historyRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\record_historyResource;
use Response;

/**
 * Class record_historyController
 * @package App\Http\Controllers\API
 */

class record_historyAPIController extends AppBaseController
{
    /** @var  record_historyRepository */
    private $recordHistoryRepository;

    public function __construct(record_historyRepository $recordHistoryRepo)
    {
        $this->recordHistoryRepository = $recordHistoryRepo;
    }

    /**
     * Display a listing of the record_history.
     * GET|HEAD /recordHistories
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $recordHistories = $this->recordHistoryRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(record_historyResource::collection($recordHistories), 'Record Histories retrieved successfully');
    }

    /**
     * Store a newly created record_history in storage.
     * POST /recordHistories
     *
     * @param Createrecord_historyAPIRequest $request
     *
     * @return Response
     */
    public function store(Createrecord_historyAPIRequest $request)
    {
        $input = $request->all();

        $recordHistory = $this->recordHistoryRepository->create($input);

        return $this->sendResponse(new record_historyResource($recordHistory), 'Record History saved successfully');
    }

    /**
     * Display the specified record_history.
     * GET|HEAD /recordHistories/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var record_history $recordHistory */
        $recordHistory = $this->recordHistoryRepository->find($id);

        if (empty($recordHistory)) {
            return $this->sendError('Record History not found');
        }

        return $this->sendResponse(new record_historyResource($recordHistory), 'Record History retrieved successfully');
    }

    /**
     * Update the specified record_history in storage.
     * PUT/PATCH /recordHistories/{id}
     *
     * @param int $id
     * @param Updaterecord_historyAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updaterecord_historyAPIRequest $request)
    {
        $input = $request->all();

        /** @var record_history $recordHistory */
        $recordHistory = $this->recordHistoryRepository->find($id);

        if (empty($recordHistory)) {
            return $this->sendError('Record History not found');
        }

        $recordHistory = $this->recordHistoryRepository->update($input, $id);

        return $this->sendResponse(new record_historyResource($recordHistory), 'record_history updated successfully');
    }

    /**
     * Remove the specified record_history from storage.
     * DELETE /recordHistories/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var record_history $recordHistory */
        $recordHistory = $this->recordHistoryRepository->find($id);

        if (empty($recordHistory)) {
            return $this->sendError('Record History not found');
        }

        $recordHistory->delete();

        return $this->sendSuccess('Record History deleted successfully');
    }
}
