<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreaterecordAPIRequest;
use App\Http\Requests\API\UpdaterecordAPIRequest;
use App\Models\record;
use App\Repositories\recordRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\recordResource;
use Response;

/**
 * Class recordController
 * @package App\Http\Controllers\API
 */

class recordAPIController extends AppBaseController
{
    /** @var  recordRepository */
    private $recordRepository;

    public function __construct(recordRepository $recordRepo)
    {
        $this->recordRepository = $recordRepo;
    }

    /**
     * Display a listing of the record.
     * GET|HEAD /records
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $records = $this->recordRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(recordResource::collection($records), 'Records retrieved successfully');
    }

    /**
     * Store a newly created record in storage.
     * POST /records
     *
     * @param CreaterecordAPIRequest $request
     *
     * @return Response
     */
    public function store(CreaterecordAPIRequest $request)
    {
        $input = $request->all();

        $record = $this->recordRepository->create($input);

        return $this->sendResponse(new recordResource($record), 'Record saved successfully');
    }

    /**
     * Display the specified record.
     * GET|HEAD /records/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var record $record */
        $record = $this->recordRepository->find($id);

        if (empty($record)) {
            return $this->sendError('Record not found');
        }

        return $this->sendResponse(new recordResource($record), 'Record retrieved successfully');
    }

    /**
     * Update the specified record in storage.
     * PUT/PATCH /records/{id}
     *
     * @param int $id
     * @param UpdaterecordAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdaterecordAPIRequest $request)
    {
        $input = $request->all();

        /** @var record $record */
        $record = $this->recordRepository->find($id);

        if (empty($record)) {
            return $this->sendError('Record not found');
        }

        $record = $this->recordRepository->update($input, $id);

        return $this->sendResponse(new recordResource($record), 'record updated successfully');
    }

    /**
     * Remove the specified record from storage.
     * DELETE /records/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var record $record */
        $record = $this->recordRepository->find($id);

        if (empty($record)) {
            return $this->sendError('Record not found');
        }

        $record->delete();

        return $this->sendSuccess('Record deleted successfully');
    }
}
