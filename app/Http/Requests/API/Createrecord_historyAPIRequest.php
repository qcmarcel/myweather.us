<?php

namespace App\Http\Requests\API;

use App\Models\record_history;
use InfyOm\Generator\Request\APIRequest;

class Createrecord_historyAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return record_history::$rules;
    }
}
