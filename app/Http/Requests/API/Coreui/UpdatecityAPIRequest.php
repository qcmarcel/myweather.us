<?php

namespace App\Http\Requests\API\Coreui;

use App\Models\Coreui\city;
use InfyOm\Generator\Request\APIRequest;

class UpdatecityAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = city::$rules;
        
        return $rules;
    }
}
