<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class locationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'location_id' => $this->location_id,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
            'area' => $this->area,
            'mapcode' => $this->mapcode,
            'city_nameid' => $this->city_nameid,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
