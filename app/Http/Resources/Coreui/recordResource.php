<?php

namespace App\Http\Resources\Coreui;

use Illuminate\Http\Resources\Json\JsonResource;

class recordResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'record_id' => $this->record_id,
            'date' => $this->date,
            'city_nameid' => $this->city_nameid,
            'value' => $this->value,
            'metric_nameid' => $this->metric_nameid,
            'history_id' => $this->history_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
