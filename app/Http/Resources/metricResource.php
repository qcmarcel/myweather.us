<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class metricResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'nameid' => $this->nameid,
            'iso' => $this->iso,
            'display_name' => $this->display_name,
            'estatus' => $this->estatus,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
