<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class record_historyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'history_id' => $this->history_id,
            'date' => $this->date,
            'ressume' => $this->ressume,
            'estatus' => $this->estatus,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
