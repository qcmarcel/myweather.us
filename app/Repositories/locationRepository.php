<?php

namespace App\Repositories;

use App\Models\location;
use App\Repositories\BaseRepository;

/**
 * Class locationRepository
 * @package App\Repositories
 * @version January 27, 2021, 10:12 am UTC
*/

class locationRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'latitude',
        'longitude',
        'area',
        'mapcode',
        'city_nameid',
        'created_at',
        'updated_at'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return location::class;
    }
}
