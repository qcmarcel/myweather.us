<?php

namespace App\Repositories;

use App\Models\city;
use App\Repositories\BaseRepository;

/**
 * Class cityRepository
 * @package App\Repositories
 * @version January 27, 2021, 10:05 am UTC
*/

class cityRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nameid',
        'iso',
        'display_name',
        'estatus',
        'created_at',
        'updated_at'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return city::class;
    }
}
