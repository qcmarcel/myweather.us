<?php

namespace App\Repositories\Coreui;

use App\Models\Coreui\metric;
use App\Repositories\BaseRepository;

/**
 * Class metricRepository
 * @package App\Repositories\Coreui
 * @version January 27, 2021, 9:39 am UTC
*/

class metricRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nameid',
        'iso',
        'display_name',
        'estatus',
        'created_at',
        'updated_at'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return metric::class;
    }
}
