<?php

namespace App\Repositories\Coreui;

use App\Models\Coreui\location;
use App\Repositories\BaseRepository;

/**
 * Class locationRepository
 * @package App\Repositories\Coreui
 * @version January 27, 2021, 9:35 am UTC
*/

class locationRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'latitude',
        'longitude',
        'area',
        'mapcode',
        'city_nameid',
        'created_at',
        'updated_at'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return location::class;
    }
}
