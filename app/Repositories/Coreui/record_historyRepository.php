<?php

namespace App\Repositories\Coreui;

use App\Models\Coreui\record_history;
use App\Repositories\BaseRepository;

/**
 * Class record_historyRepository
 * @package App\Repositories\Coreui
 * @version January 27, 2021, 9:42 am UTC
*/

class record_historyRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'date',
        'ressume',
        'estatus',
        'created_at',
        'updated_at'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return record_history::class;
    }
}
