<?php

namespace App\Repositories\Coreui;

use App\Models\Coreui\record;
use App\Repositories\BaseRepository;

/**
 * Class recordRepository
 * @package App\Repositories\Coreui
 * @version January 27, 2021, 9:48 am UTC
*/

class recordRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'date',
        'city_nameid',
        'value',
        'metric_nameid',
        'history_id',
        'created_at',
        'updated_at'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return record::class;
    }
}
