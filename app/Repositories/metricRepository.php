<?php

namespace App\Repositories;

use App\Models\metric;
use App\Repositories\BaseRepository;

/**
 * Class metricRepository
 * @package App\Repositories
 * @version January 27, 2021, 10:13 am UTC
*/

class metricRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nameid',
        'iso',
        'display_name',
        'estatus',
        'created_at',
        'updated_at'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return metric::class;
    }
}
