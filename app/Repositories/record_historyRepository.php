<?php

namespace App\Repositories;

use App\Models\record_history;
use App\Repositories\BaseRepository;

/**
 * Class record_historyRepository
 * @package App\Repositories
 * @version January 27, 2021, 10:14 am UTC
*/

class record_historyRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'date',
        'ressume',
        'estatus',
        'created_at',
        'updated_at'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return record_history::class;
    }
}
