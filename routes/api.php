<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::resource('cities', App\Http\Controllers\API\cityAPIController::class);


Route::resource('locations', App\Http\Controllers\API\locationAPIController::class);


Route::resource('metrics', App\Http\Controllers\API\metricAPIController::class);


Route::resource('record_histories', App\Http\Controllers\API\record_historyAPIController::class);


Route::resource('records', App\Http\Controllers\API\recordAPIController::class);

